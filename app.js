// const request = require("request-promise");
const axios = require("axios");
const unzip = require("unzip");
const fs = require("fs");
const csv = require("csvtojson");
const { sequelize, models } = require("./sequal");

async function main() {
  let dumpurl = await axios
    .get("https://global.bridgeapp.com/api/admin/data_dumps/download", {
      validateStatus: status => status === 200,
      headers: {
        Authorization:
          "Basic YTE1YTVmM2QtNGU5Yy00MzJiLTgxMjYtYzlhYTRhMmU5OWFiOmJkYWNjODY1LWE3NzYtNDE2OC05MTQ3LTJkMjVkYzJiZTkyNg=="
      }
    })
    .then(res => {
      return res.request.res.responseUrl;
    })
    .catch(err => {
      return err.request.res.responseUrl;
    });

  dumpurl = decodeURIComponent(dumpurl);

  const resp = await axios
    .get(dumpurl, {
      validateStatus: status => status === 200,
      responseType: "stream"
    })
    .then(({ data: stream }) => {
      stream.pipe(unzip.Extract({ path: "output/" }));

      return new Promise((resolve, reject) => {
        stream.on("end", () => {
          resolve("done unzip");
        });
      });
    })
    .catch(error => {
      console.log(error.response.data);
    });

  fs.readdirSync("./output/").forEach(async (file, index) => {
    console.log(file);
    csv()
      .fromFile(`./output/${file}`)
      .then(jsonObj => {
        const fileReplaced = file.replace(".csv", "");
        const model = models[fileReplaced];
        jsonObj.map(item => {
          model.upsert(item);
        });
        // console.log(model);
      });
  });

  return;
}

main();

// curl -XGET -H 'Authorization: AWS AKIAIASJNIACY3Z5L2BA:DCCbrN5%2BxfCA3rnFzssyw9BZ4sU%3D' 'https://s3.amazonaws.com/bridgelms-production-appbucket1-mll0x52cmp4b/6107/1/downloads/global_data_dump_20191127201555.zip?AWSAccessKeyId=AKIAIASJNIACY3Z5L2BA&amp;Expires=1575009801&amp;Signature=DCCbrN5%2BxfCA3rnFzssyw9BZ4sU%3D'

// curl -XGET -H 'Authorization: AWS AKIAIASJNIACY3Z5L2BA:eqFGZN8hPqH8Y%2FSb1UseqXlyNYI%3D' -H 'X-Amz-Date: Fri, 29 Nov 2019 08:47:41  +0000' 'https://s3.amazonaws.com/bridgelms-production-appbucket1-mll0x52cmp4b/6107/1/downloads/global_data_dump_20191127201555.zip'
