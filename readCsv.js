// const readXlsxFile = require("read-excel-file/node");
const fs = require("fs");
var toString = require("stream-to-string");
var csvHeaders = require("csv-headers");

async function main() {
  var templateFile = fs.createReadStream("./models/templateFile.js");
  var templateContent = await bufToStr(templateFile).then(res => res);

  var sequalTemp = fs.createReadStream("./sequalTemp.js");
  var sequalContent = await bufToStr(sequalTemp).then(res => res);

  var filesList = [];
  var modelList = [];
  var fileNames = [];

  fs.readdirSync("./files").forEach(async (file, index) => {
    const fileReplaced = file.replace(".csv", "");

    fileNames.push(fileReplaced);

    filesList.push(
      `var ${fileReplaced} = require("./newmodels/${fileReplaced}")`
    );

    modelList.push(`${fileReplaced} = ${fileReplaced}(sequelize&Sequelize)`);

    var options = {
      file: `${__dirname}/files/${file}`,
      delimiter: ","
    };

    //   headers.map(head => {});
    var tempTemplate = templateContent;
    tempTemplate = tempTemplate.replace("tableName", file.replace(".csv", ""));
    tempTemplate = replaceAll(
      tempTemplate,
      "modelName",
      file.replace(".csv", "").toUpperCase()
    );

    csvHeaders(options, function(err, headers) {
      const modelItems = headers.map(item => {
        if (item == "id")
          return "id: {primaryKey: true,type: DATATYPE.INTEGER}";
        else return `${item}: { type: DATATYPE.STRING }`;
      });
      tempTemplate = tempTemplate.replace("placeholder", modelItems);
      tempTemplate = replaceAll(tempTemplate, '"', "");
      fs.writeFile(
        `newmodels/${file.replace(".csv", "")}.js`,
        tempTemplate,
        function(err) {
          if (err) {
            return console.log(err);
          }
        }
      );
    });
  });

  // To create Sequal file to import models, create tables and export models
  let filesString = filesList.toString();
  filesString = replaceAll(filesString, ",", "\n");
  let modelString = modelList.toString();
  modelString = replaceAll(modelString, ",", "\n");
  modelString = replaceAll(modelString, "&", ",");

  modelString = sequalContent.replace("placeholder", modelString);
  modelString = modelString.replace("modelImports", filesString);
  modelString = modelString.replace(
    "exportPlace",
    `{ seq:sequelize, ${fileNames.map(item => `"${item}":${item}`)} }`
  );

  fs.writeFile(`modelInitialize.js`, modelString, function(err) {
    if (err) {
      return console.log(err);
    }
  });

  // end
}

async function bufToStr(stream) {
  const data = await toString(stream).then(function(msg) {
    return msg;
  });

  return data;
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, "g"), replace);
}

main();
