
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('tag_relevances', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},tag_id: { type: DATATYPE.STRING },enrollable_id: { type: DATATYPE.STRING },enrollable_type: { type: DATATYPE.STRING },relevance: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

