
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('lti_placements', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},placement: { type: DATATYPE.STRING },lti_tool_id: { type: DATATYPE.STRING },url: { type: DATATYPE.STRING },icon_url: { type: DATATYPE.STRING },text: { type: DATATYPE.STRING },bridge_icon_class: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING }
    })
}

