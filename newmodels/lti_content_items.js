
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('lti_content_items', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},content_item_type: { type: DATATYPE.STRING },media_type: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },item_id: { type: DATATYPE.STRING },url: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },lti_tool_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },enrollments_count: { type: DATATYPE.STRING }
    })
}

