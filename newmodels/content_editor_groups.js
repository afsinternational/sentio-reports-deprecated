
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('content_editor_groups', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},content_id: { type: DATATYPE.STRING },content_type: { type: DATATYPE.STRING },group_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

