
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('attempts', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},state: { type: DATATYPE.STRING },started_at: { type: DATATYPE.STRING },ended_at: { type: DATATYPE.STRING },progress: { type: DATATYPE.STRING },score: { type: DATATYPE.STRING },current_slide_id: { type: DATATYPE.STRING },enrollment_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },furthest_slide_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

