
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('integration_fields', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },vendor: { type: DATATYPE.STRING },feature: { type: DATATYPE.STRING },contents: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

