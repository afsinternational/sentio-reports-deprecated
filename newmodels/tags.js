
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('tags', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },taggings_count: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },tag_type: { type: DATATYPE.STRING },image_url: { type: DATATYPE.STRING },locale: { type: DATATYPE.STRING }
    })
}

