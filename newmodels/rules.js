
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('rules', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},custom_field_id: { type: DATATYPE.STRING },predicate: { type: DATATYPE.STRING },group_id: { type: DATATYPE.STRING },value: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

