
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('program_enrollment_sources', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},program_enrollment_id: { type: DATATYPE.STRING },enrollable_id: { type: DATATYPE.STRING },enrollable_type: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

