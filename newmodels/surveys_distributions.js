
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_distributions', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},survey_id: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },distribution_type: { type: DATATYPE.STRING },closes_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },recipients_count: { type: DATATYPE.STRING },responses_count: { type: DATATYPE.STRING }
    })
}

