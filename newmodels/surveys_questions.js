
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_questions', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},survey_id: { type: DATATYPE.STRING },position: { type: DATATYPE.STRING },data: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },required: { type: DATATYPE.STRING }
    })
}

