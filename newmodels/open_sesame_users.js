
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('open_sesame_users', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},tenant_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },uid: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

