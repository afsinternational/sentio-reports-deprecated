
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_course_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },live_course_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },sources_count: { type: DATATYPE.STRING },expires_at: { type: DATATYPE.STRING },inactive: { type: DATATYPE.STRING },renew_by: { type: DATATYPE.STRING }
    })
}

