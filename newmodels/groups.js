
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('groups', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },parent_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },memberships_count: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },type: { type: DATATYPE.STRING },archived_at: { type: DATATYPE.STRING },join_type: { type: DATATYPE.STRING },new_smart_group: { type: DATATYPE.STRING },external_id: { type: DATATYPE.STRING }
    })
}

