
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('grants', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },role_id: { type: DATATYPE.STRING }
    })
}

