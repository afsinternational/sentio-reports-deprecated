
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('scorm_dispatches', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },course_template_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },external_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

