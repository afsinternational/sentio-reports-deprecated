
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_recipient_group_members', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},recipient_group_id: { type: DATATYPE.STRING },recipient_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

