
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('attachments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},notorious_id: { type: DATATYPE.STRING },location: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },duration: { type: DATATYPE.STRING },name: { type: DATATYPE.STRING },learner_visible: { type: DATATYPE.STRING },confirmed: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },context_id: { type: DATATYPE.STRING },context_type: { type: DATATYPE.STRING },content_type: { type: DATATYPE.STRING },file_modified_at: { type: DATATYPE.STRING },availability: { type: DATATYPE.STRING }
    })
}

