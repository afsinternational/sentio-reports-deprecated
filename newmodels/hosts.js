
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('hosts', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},account_id: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },subdomain: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },vanity_domain: { type: DATATYPE.STRING },vanity_domain_state: { type: DATATYPE.STRING },talent_vanity_domain: { type: DATATYPE.STRING }
    })
}

