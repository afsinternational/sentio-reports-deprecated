
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('content_providers', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },enabled: { type: DATATYPE.STRING },config_errors: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },last_synced_at: { type: DATATYPE.STRING }
    })
}

