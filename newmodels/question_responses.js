
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('question_responses', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},slide_id: { type: DATATYPE.STRING },score: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },attempt_id: { type: DATATYPE.STRING },answers: { type: DATATYPE.STRING },question: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },answer: { type: DATATYPE.STRING }
    })
}

