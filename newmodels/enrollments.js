
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },completed_at: { type: DATATYPE.STRING },score: { type: DATATYPE.STRING },end_at: { type: DATATYPE.STRING },course_template_id: { type: DATATYPE.STRING },attempts_count: { type: DATATYPE.STRING },required: { type: DATATYPE.STRING },external_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING },expires_at: { type: DATATYPE.STRING },inactive: { type: DATATYPE.STRING },renew_by: { type: DATATYPE.STRING },archived_at: { type: DATATYPE.STRING }
    })
}

