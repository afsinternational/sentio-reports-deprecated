
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('program_sections', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},title: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },unpublished_changes: { type: DATATYPE.STRING }
    })
}

