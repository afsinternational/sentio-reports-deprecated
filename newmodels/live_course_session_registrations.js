
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_course_session_registrations', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},live_course_session_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },live_course_enrollment_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING },marked_complete_at: { type: DATATYPE.STRING }
    })
}

