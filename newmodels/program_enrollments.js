
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('program_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },program_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },start_at: { type: DATATYPE.STRING },end_at: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },average_score: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },sources_count: { type: DATATYPE.STRING },completed_program_item_ids: { type: DATATYPE.STRING },program_item_count: { type: DATATYPE.STRING },expires_at: { type: DATATYPE.STRING },renew_by: { type: DATATYPE.STRING },inactive: { type: DATATYPE.STRING },completed_at: { type: DATATYPE.STRING }
    })
}

