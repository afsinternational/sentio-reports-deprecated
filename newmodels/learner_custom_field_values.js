
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('learner_custom_field_values', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },custom_field_id: { type: DATATYPE.STRING },value: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

