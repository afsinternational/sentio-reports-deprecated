
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_submissions', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},distribution_id: { type: DATATYPE.STRING },recipient_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },submitted_at: { type: DATATYPE.STRING }
    })
}

