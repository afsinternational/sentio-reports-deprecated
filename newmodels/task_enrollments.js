
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('task_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },task_id: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },sources_count: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },completed_at: { type: DATATYPE.STRING },attachments_count: { type: DATATYPE.STRING },end_at: { type: DATATYPE.STRING },inactive: { type: DATATYPE.STRING },approver_user_id: { type: DATATYPE.STRING }
    })
}

