
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_course_session_instructors', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },live_course_session_id: { type: DATATYPE.STRING },name: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

