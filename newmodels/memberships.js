
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('memberships', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},group_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },enrollments_synced_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

