
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('library_items', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},item_id: { type: DATATYPE.STRING },item_type: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },estimated_time: { type: DATATYPE.STRING },features: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },published_at: { type: DATATYPE.STRING },cover_slide_data: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },data: { type: DATATYPE.STRING }
    })
}

