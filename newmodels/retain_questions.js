
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('retain_questions', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},retain_quiz_id: { type: DATATYPE.STRING },slide_id: { type: DATATYPE.STRING },question: { type: DATATYPE.STRING },answer: { type: DATATYPE.STRING },score: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },position: { type: DATATYPE.STRING }
    })
}

