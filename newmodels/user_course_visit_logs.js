
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('user_course_visit_logs', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},token: { type: DATATYPE.STRING },enrollment_id: { type: DATATYPE.STRING },date: { type: DATATYPE.STRING },seconds: { type: DATATYPE.STRING },ip_address: { type: DATATYPE.STRING },user_agent: { type: DATATYPE.STRING },device_type: { type: DATATYPE.STRING },tracked_seconds: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

