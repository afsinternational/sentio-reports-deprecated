
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('library_item_sources', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},library_item_id: { type: DATATYPE.STRING },enrollable_id: { type: DATATYPE.STRING },enrollable_type: { type: DATATYPE.STRING },learnable_id: { type: DATATYPE.STRING },learnable_type: { type: DATATYPE.STRING },relevance: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

