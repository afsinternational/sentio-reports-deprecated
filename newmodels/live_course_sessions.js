
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_course_sessions', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},start_at: { type: DATATYPE.STRING },end_at: { type: DATATYPE.STRING },location: { type: DATATYPE.STRING },seats: { type: DATATYPE.STRING },live_course_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },registered_count: { type: DATATYPE.STRING },concluded_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },notes: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },present_count: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },parent_id: { type: DATATYPE.STRING },multi_part: { type: DATATYPE.STRING },timezone: { type: DATATYPE.STRING },published_at: { type: DATATYPE.STRING },mark_attended_on_launch: { type: DATATYPE.STRING }
    })
}

