
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_recipients', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },distribution_id: { type: DATATYPE.STRING }
    })
}

