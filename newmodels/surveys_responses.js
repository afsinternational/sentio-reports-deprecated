
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_responses', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},submission_id: { type: DATATYPE.STRING },question_id: { type: DATATYPE.STRING },data: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

