
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('devices', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},token: { type: DATATYPE.STRING },device_id: { type: DATATYPE.STRING },endpoint: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

