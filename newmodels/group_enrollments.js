
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('group_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},group_id: { type: DATATYPE.STRING },course_template_id: { type: DATATYPE.STRING },average_score: { type: DATATYPE.STRING },course_completion_count: { type: DATATYPE.STRING },learner_count: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },completion_ratio: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

