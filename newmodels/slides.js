
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('slides', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},course_template_id: { type: DATATYPE.STRING },position: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },new_position: { type: DATATYPE.STRING },published_attributes: { type: DATATYPE.STRING },draft_attributes: { type: DATATYPE.STRING },published_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },estimated_time: { type: DATATYPE.STRING },attachments_count: { type: DATATYPE.STRING },presentable: { type: DATATYPE.STRING },external_id: { type: DATATYPE.STRING }
    })
}

