
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('learning_items', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },enrollable_id: { type: DATATYPE.STRING },enrollable_type: { type: DATATYPE.STRING },learnable_id: { type: DATATYPE.STRING },learnable_type: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },completed_at: { type: DATATYPE.STRING },data: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },program_ids: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },archived_at: { type: DATATYPE.STRING }
    })
}

