
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_courses', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},title: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },sessions_count: { type: DATATYPE.STRING },attachments_count: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },external_course_id: { type: DATATYPE.STRING }
    })
}

