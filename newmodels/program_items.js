
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('program_items', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},program_id: { type: DATATYPE.STRING },position: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },item_id: { type: DATATYPE.STRING },item_type: { type: DATATYPE.STRING }
    })
}

