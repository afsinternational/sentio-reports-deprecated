
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('taggings', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},tag_id: { type: DATATYPE.STRING },taggable_id: { type: DATATYPE.STRING },taggable_type: { type: DATATYPE.STRING },tagger_id: { type: DATATYPE.STRING },tagger_type: { type: DATATYPE.STRING },context: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },data: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING }
    })
}

