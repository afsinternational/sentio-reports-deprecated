
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('users', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},email: { type: DATATYPE.STRING },uid: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },avatar_url: { type: DATATYPE.STRING },first_name: { type: DATATYPE.STRING },last_name: { type: DATATYPE.STRING },external_id: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },tac_accepted_at: { type: DATATYPE.STRING },tagline: { type: DATATYPE.STRING },locale: { type: DATATYPE.STRING },unsubscribed: { type: DATATYPE.STRING },welcomed_at: { type: DATATYPE.STRING },logged_in_at: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING },full_name: { type: DATATYPE.STRING },sortable_name: { type: DATATYPE.STRING },uuid: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },hris_id: { type: DATATYPE.STRING },support_user: { type: DATATYPE.STRING },hire_date: { type: DATATYPE.STRING },hidden: { type: DATATYPE.STRING },job_title: { type: DATATYPE.STRING },profile: { type: DATATYPE.STRING },department: { type: DATATYPE.STRING },anonymized: { type: DATATYPE.STRING }
    })
}

