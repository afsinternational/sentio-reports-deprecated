
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('temporary_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},import_object_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },learnable_type: { type: DATATYPE.STRING },learnable_id: { type: DATATYPE.STRING },data: { type: DATATYPE.STRING }
    })
}

