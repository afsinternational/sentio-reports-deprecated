
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('custom_fields', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING }
    })
}

