
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('enrollment_profiles', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},enrollable_id: { type: DATATYPE.STRING },enrollable_type: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },auto_re_enroll: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING },uuid: { type: DATATYPE.STRING },expires: { type: DATATYPE.STRING },open_enrollment: { type: DATATYPE.STRING }
    })
}

