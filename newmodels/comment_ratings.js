
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('comment_ratings', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},comment_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },rating: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

