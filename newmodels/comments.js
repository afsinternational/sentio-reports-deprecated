
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('comments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },body: { type: DATATYPE.STRING },commentable_id: { type: DATATYPE.STRING },commentable_type: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },attachment_url: { type: DATATYPE.STRING },rating_sum: { type: DATATYPE.STRING },parent_id: { type: DATATYPE.STRING },children_count: { type: DATATYPE.STRING },edited_at: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },attachment_alt_text: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

