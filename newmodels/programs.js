
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('programs', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},title: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },assignment_period: { type: DATATYPE.STRING },published_at: { type: DATATYPE.STRING },unpublished_changes: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },learner_enrollments_count: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },graphic: { type: DATATYPE.STRING },non_linear: { type: DATATYPE.STRING },step_notifications: { type: DATATYPE.STRING },has_certificate: { type: DATATYPE.STRING },external_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },item_count: { type: DATATYPE.STRING }
    })
}

