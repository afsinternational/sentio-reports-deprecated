
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_surveys', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},domain_id: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },anonymous: { type: DATATYPE.STRING },publish_results: { type: DATATYPE.STRING },distributions_count: { type: DATATYPE.STRING }
    })
}

