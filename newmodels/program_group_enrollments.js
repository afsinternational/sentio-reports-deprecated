
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('program_group_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},group_id: { type: DATATYPE.STRING },program_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

