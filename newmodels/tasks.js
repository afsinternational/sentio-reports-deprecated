
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('tasks', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},title: { type: DATATYPE.STRING },instructions: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },enrollments_count: { type: DATATYPE.STRING },has_certificate: { type: DATATYPE.STRING },attachments_count: { type: DATATYPE.STRING },draft_attributes: { type: DATATYPE.STRING },published_attributes: { type: DATATYPE.STRING },published_at: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING }
    })
}

