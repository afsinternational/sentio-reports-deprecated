
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('lti_content_item_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},user_id: { type: DATATYPE.STRING },lti_content_item_id: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },sources_count: { type: DATATYPE.STRING },completed_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },inactive: { type: DATATYPE.STRING }
    })
}

