
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('import_objects', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},file: { type: DATATYPE.STRING },context_id: { type: DATATYPE.STRING },context_type: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },completed: { type: DATATYPE.STRING },total: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },new_user_count: { type: DATATYPE.STRING },deleted_user_count: { type: DATATYPE.STRING },updated_user_count: { type: DATATYPE.STRING },restored_user_count: { type: DATATYPE.STRING },ignored_user_count: { type: DATATYPE.STRING },new_custom_field_count: { type: DATATYPE.STRING },invalid_rows: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },mappings: { type: DATATYPE.STRING },has_headers: { type: DATATYPE.STRING },cleaned_at: { type: DATATYPE.STRING },auto_csv: { type: DATATYPE.STRING },benchmarks: { type: DATATYPE.STRING },counts: { type: DATATYPE.STRING }
    })
}

