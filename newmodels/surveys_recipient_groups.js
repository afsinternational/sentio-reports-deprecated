
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('surveys_recipient_groups', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},distribution_id: { type: DATATYPE.STRING },name: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },group_id: { type: DATATYPE.STRING }
    })
}

