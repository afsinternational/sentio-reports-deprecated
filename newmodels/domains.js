
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('domains', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },parent_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },config: { type: DATATYPE.STRING },owner_id: { type: DATATYPE.STRING },active: { type: DATATYPE.STRING },users_count: { type: DATATYPE.STRING },course_templates_count: { type: DATATYPE.STRING },domain_type: { type: DATATYPE.STRING },tac_type: { type: DATATYPE.STRING },tac_custom_body: { type: DATATYPE.STRING },tac_updated_at: { type: DATATYPE.STRING },tac_custom_body_markup: { type: DATATYPE.STRING },immutable_uuid: { type: DATATYPE.STRING }
    })
}

