
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('local_saml_service_providers', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },metadata: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },sub_account_id: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },content_provider_id: { type: DATATYPE.STRING }
    })
}

