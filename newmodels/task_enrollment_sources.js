
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('task_enrollment_sources', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},task_enrollment_id: { type: DATATYPE.STRING },enrollable_id: { type: DATATYPE.STRING },enrollable_type: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

