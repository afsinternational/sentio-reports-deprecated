
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_course_group_enrollments', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},group_id: { type: DATATYPE.STRING },live_course_id: { type: DATATYPE.STRING },registered_count: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

