
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('live_course_web_conferences', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},provider: { type: DATATYPE.STRING },other_provider: { type: DATATYPE.STRING },meeting_url: { type: DATATYPE.STRING },access_code: { type: DATATYPE.STRING },phone: { type: DATATYPE.STRING },host_key: { type: DATATYPE.STRING },password: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },live_course_session_id: { type: DATATYPE.STRING },user_id: { type: DATATYPE.STRING },registration_link: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

