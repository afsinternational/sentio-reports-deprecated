
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('retain_quizzes', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},enrollment_id: { type: DATATYPE.STRING },current_question_id: { type: DATATYPE.STRING },state: { type: DATATYPE.STRING },progress: { type: DATATYPE.STRING },score: { type: DATATYPE.STRING },send_at: { type: DATATYPE.STRING },started_at: { type: DATATYPE.STRING },ended_at: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING }
    })
}

