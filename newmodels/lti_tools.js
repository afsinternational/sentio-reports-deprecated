
module.exports = (sequelize, DATATYPE) => {
  return sequelize.define('lti_tools', {
      id: {primaryKey: true,type: DATATYPE.INTEGER},name: { type: DATATYPE.STRING },description: { type: DATATYPE.STRING },consumer_key: { type: DATATYPE.STRING },xml_configuration: { type: DATATYPE.STRING },created_at: { type: DATATYPE.STRING },updated_at: { type: DATATYPE.STRING },launch_url: { type: DATATYPE.STRING },secure_launch_url: { type: DATATYPE.STRING },icon: { type: DATATYPE.STRING },secure_icon: { type: DATATYPE.STRING },cartridge_bundle: { type: DATATYPE.STRING },cartridge_icon: { type: DATATYPE.STRING },tool_enabled: { type: DATATYPE.STRING },custom_fields: { type: DATATYPE.STRING },config_url: { type: DATATYPE.STRING },domain_id: { type: DATATYPE.STRING },title: { type: DATATYPE.STRING },managed_internally: { type: DATATYPE.STRING },deleted_at: { type: DATATYPE.STRING }
    })
}

