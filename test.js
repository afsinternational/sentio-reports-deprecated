const sequelize = require(sequelize);

var ATTACHMENTS = sequelize.define(
  "attachments",
  {
    id: { type: Sequelize.INTEGER },
    notorious_id: { type: Sequelize.INTEGER },
    location: { type: Sequelize.INTEGER },
    created_at: { type: Sequelize.INTEGER },
    updated_at: { type: Sequelize.INTEGER },
    deleted_at: { type: Sequelize.INTEGER },
    duration: { type: Sequelize.INTEGER },
    name: { type: Sequelize.INTEGER },
    learner_visible: { type: Sequelize.INTEGER },
    confirmed: { type: Sequelize.INTEGER },
    user_id: { type: Sequelize.INTEGER },
    context_id: { type: Sequelize.INTEGER },
    context_type: { type: Sequelize.INTEGER },
    content_type: { type: Sequelize.INTEGER },
    file_modified_at: { type: Sequelize.INTEGER },
    availability: { type: Sequelize.INTEGER }
  },
  {
    freezeTableName: true // Model tableName will be the same as the model name
  }
);

module.exports = ATTACHMENTS;
